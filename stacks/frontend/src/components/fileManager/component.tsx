// tslint:disable-next-line:no-submodule-imports
const uploadIcon = require('material-design-icons/file/svg/production/ic_file_upload_48px.svg');

import axios from 'axios';
import * as React from 'react';
import { OutboundLink } from 'react-ga';

import { getArticleUploadUrl } from '../../services/periodical';

interface UploadedFile {
  contentUrl: string;
  name: string;
}
interface FileManagerState {
  formValues: {
    agreeWithLicense: boolean;
    file?: File;
  };
  uploadedFile?: UploadedFile;
  isUploading: boolean;
  error?: JSX.Element;
}
export interface FileManagerProps {
  articleId: string;
  files: UploadedFile[];
  onUpload?: (file: UploadedFile & { license: 'https://creativecommons.org/licenses/by/4.0/' }) => void;
}

export class FileManager
  extends React.Component<
  FileManagerProps,
  FileManagerState
> {
  public state: FileManagerState = {
    formValues: {
      agreeWithLicense: false,
    },
    isUploading: false,
  };

  constructor(props: FileManagerProps) {
    super(props);

    this.handleFileSelect = this.handleFileSelect.bind(this);
    this.handleLicenseCheck = this.handleLicenseCheck.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.renderFile = this.renderFile.bind(this);
    this.renderNotifications = this.renderNotifications.bind(this);
  }

  public render() {
    return (
      <div>
        {this.renderFile()}
        <form onSubmit={this.handleUpload}>
          <div className="field">
            <div className="file is-fullwidth has-name">
              <label className="file-label">
                <input
                  onChange={this.handleFileSelect}
                  accept="application/pdf"
                  type="file"
                  name="article"
                  className="file-input"
                />
                <span className="file-cta">
                  <span className="file-icon">
                    <img src={uploadIcon} alt=""/>
                  </span>
                  <span className="file-label">
                    Select a PDF file&hellip;
                  </span>
                </span>
                <span className="file-name">
                  {(this.state.formValues.file) ? this.state.formValues.file.name : null}
                </span>
              </label>
            </div>
          </div>
          <div className="field">
            <div className="control">
              <label className="checkbox">
                <input onChange={this.handleLicenseCheck} type="checkbox"/>&nbsp;
                I want to make this document public under a&nbsp;
                <OutboundLink
                  to="https://creativecommons.org/licenses/by/4.0/"
                  title="View the full terms of the license"
                  rel="license"
                  eventLabel="cc-license"
                >
                  CC-BY license
                </OutboundLink>, and have the rights to do so.
              </label>
            </div>
          </div>
          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <button
                type="submit"
                disabled={!this.state.formValues.agreeWithLicense || !this.state.formValues.file}
                className={`button is-link ${this.state.isUploading ? 'is-loading' : ''}`}
              >
                Upload
              </button>
            </div>
          </div>
        </form>
        {this.renderNotifications()}
      </div>
    );
  }

  private renderFile() {
    const file = this.state.uploadedFile || this.props.files[0];
    if (!file) {
      return null;
    }

    return (
      <p className="box has-text-centered">
        <a
          href={file.contentUrl}
          download={file.name}
          title="Download this file"
          className="button is-text is-large"
        >
          {file.name}
        </a>
      </p>
    );
  }

  private renderNotifications() {
    if (this.state.error) {
      return (
        <div className="section">
          <div className="message is-danger">
            <div className="message-body">
              {this.state.error}
            </div>
          </div>
        </div>
      );
    }

    return null;
  }

  private handleFileSelect(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    this.setState((prevState: FileManagerState) => ({
      formValues: {
        ...prevState.formValues,
        file: (target.files && target.files.length === 1) ? target.files[0] : undefined,
      },
    }));
  }

  private handleLicenseCheck(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    this.setState((prevState: FileManagerState) => ({
      formValues: {
        ...prevState.formValues,
        agreeWithLicense: target.checked,
      },
    }));
  }

  private async handleUpload(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (!this.state.formValues.file) {
      return;
    }
    const file = this.state.formValues.file;

    if (!this.state.formValues.agreeWithLicense) {
      this.setState({
        error: <span>You can only upload documents if you agree with the license.</span>,
      });

      return;
    }

    this.setState({ isUploading: true });

    try {
      const mediaObject = await getArticleUploadUrl(
        this.props.articleId,
        file.name,
        this.state.formValues.agreeWithLicense,
      );

      // Unfortunately, I couldn't get this to work using the Fetch API.
      // Using Axios is a bit of a cop-out, but it works (presumably due to being built
      // on XMLHTTPRequest), and has other advantages that we might make us of in the future.
      await axios.put(mediaObject.object.toLocation, file);

      const uploadedFile = {
        contentUrl: mediaObject.result.associatedMedia.contentUrl,
        name: file.name,
      };
      this.setState({
        error: undefined,
        isUploading: false,
        uploadedFile,
      });

      if (typeof this.props.onUpload === 'function') {
        this.props.onUpload({
          contentUrl: uploadedFile.contentUrl,
          license: 'https://creativecommons.org/licenses/by/4.0/',
          name: uploadedFile.name,
        });
      }
    } catch (e) {
      this.setState({
        error: <span>Something went wrong uploading your document, please try again.</span>,
        isUploading: false,
      });
    }
  }
}
