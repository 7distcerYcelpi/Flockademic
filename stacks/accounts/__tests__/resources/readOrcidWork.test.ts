jest.mock(
  'node-fetch',
  () => ({
    default: jest.fn().mockReturnValue(Promise.resolve({
      json: () => Promise.resolve({ data: 'arbitrary orcid data' }),
    })),
  }),
);

import { readOrcidWork } from '../../src/resources/readOrcidWork';

const mockContext = {
  body: {},

  headers: {},
  method: 'GET' as 'GET',
  params: [ '/orcid/arbitrary_orcid/work/arbitrary_putcode', 'arbitrary_orcid', 'arbitrary_putcode' ],
  path: '/orcid/arbitrary_orcid/work/arbitrary_putcode',
  query: null,
};

beforeEach(() => {
  process.env.orcid_base_path = 'https://arbitrary.orcid.basepath';
});

it('should return the ORCID response when passed the correct values', () => {
  return expect(readOrcidWork(mockContext)).resolves.toHaveProperty('data');
});

it('should error when no put ORCID and code were specified', () => {
  return expect(readOrcidWork({
    ...mockContext,
    params: [],
    path: '/',
  })).rejects
    .toEqual(new Error('Please specify an ORCID and a put-code of a publication to fetch.'));
});

it('should error when the ORCID API returns an error', () => {
  const mockedFetch = require.requireMock('node-fetch').default.mockReturnValueOnce(
    Promise.resolve({ json: () => Promise.resolve({ error: 'Arbitrary error' }) }),
  );

  return expect(readOrcidWork(mockContext)).rejects
    .toEqual(new Error('Could not fetch this ORCID work, please try again.'));
});

it('should call to the sandbox public API when the member API is also pointing to the sandbox', () => {
  const mockedFetch = require.requireMock('node-fetch').default;
  process.env.orcid_base_path = 'https://arbitrary.sandbox.orcid.org.basepath';

  readOrcidWork(mockContext);

  expect(mockedFetch.mock.calls[0][0]).toMatch('.sandbox');
});
